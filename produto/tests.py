from django.test import TestCase
from .models import Product
from django.core.urlresolvers import reverse
# Create your tests here.

class ProductModelTest(TestCase):

	def test_save_product_as_priority(self):
		Product.objects.create(id=1, name="Produto Teste", priority=False)
		product = Product.objects.get(pk=1)
		product.priority = True
		product.save()
		product_priority = Product.objects.get(pk=1)
		self.assertEqual(product_priority.priority, True)
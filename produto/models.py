
from __future__ import unicode_literals

from django.db import models

class Product(models.Model):
    name = models.CharField(max_length=500, blank=True, null=True)
    priority = models.BooleanField(default=False)

    def __str__(self):
    	return self.name

    class Meta:
        managed = True
        db_table = 'product'
from django.shortcuts import render, render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import RequestContext, loader
from .models import Product
from django.core.urlresolvers import reverse
from django.contrib.auth import views
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.

def login(request):
    template_response = views.login(request)
    return template_response

@login_required(login_url='/login/')
def index(request):
	products = product_pagination(Product.objects.filter(priority=True), request.GET.get('page'))
	context = {'products': products}
	return render_to_response('produto/index.html', context, context_instance=RequestContext(request))

@login_required(login_url='/login/')
def priority(request):
	try:
		product = get_object_or_404(Product, pk=request.POST['product_id'])
		product.priority = True
		product.save()
	except (ValueError, Http404):
		products = product_pagination(Product.objects.filter(priority=True))
		context = {'message': "ID do Produto incorreto", 'products': products}
		return render_to_response('produto/index.html', context, context_instance=RequestContext(request))

	return HttpResponseRedirect(reverse('produto:index'))

def product_pagination(products, page=0):
	paginator = Paginator(products, 10) 

	try:
		products = paginator.page(page)
	except PageNotAnInteger:
		products = paginator.page(paginator.num_pages)
	except EmptyPage:
		products = paginator.page(paginator.num_pages)

	return products
